<?php namespace App\Http\Controllers;

use App\Http\Model\Pc;
use App\Http\Service\PcService;
use Illuminate\Http\Request;
use Session;
use App\Http\Form\AddPcForm;

class PcController extends Controller {
     
     /**
      * 
      * @var App\Http\Service\PcService
      * 
      */ 
     protected $pc;
     
	public function __construct(PcService $pc)
	{
          $this->pc = $pc;
	}

     /**
      *  Функция отображает список компьютеров
      */
	public function index()
     {	
          return view('pc/pclist',['pcs' => $this->pc->getAll(),
                                   'message'=>Session::get('message')]);
	}
     
     /**
      *  Функция отображает информацию по конкретному компьютеру или возвращает страницу 404
      * 
      * @var int $id
      */
     public function show($id)
     {    $pc = $this->pc->getPc($id);
          if(!empty($pc)){
               return view('pc/pc',['pc' => $pc]);
          } else {
               return abort(404);
          }
     }
     
     /**
      *  Функция отображает форму добавления компьютера и производит обработку введенных данных
      * 
      * 
      * @var Request $request
      */
     public function add(Request $request)
     {
          if ($request->isMethod('post'))
          {               
               $form = new AddPcForm();
               $pc = $form->buildPc($request);
               
               if($pc instanceof Pc && $this->pc->addPc($pc))
               
                    return redirect('/pc')->with('message', 'Успешно Добавлено');
               else 
                    return redirect()->back()->with('message','Компьютер не добавлен');
          } else {
               return view('pc/add',['message'=>Session::get('message')]);
          }
     }
     
     /**
      *  Функция отправляет запрос на удаление компьютера
      * 
      * @var Request $request
      * @var int $id
      */
     public function remove(Request $request,$id)
     {
          if($this->pc->removePc($id))
               return redirect('/pc')->with('message', 'Запись удалена');
          else
               return redirect('/pc')->with('message', 'Во время удаления возникла ошибка');
     }
     /**
      *  Функция отображает форму редактирования компьютера и производит обработку введенных данных
      * 
      * @var Request $request
      * @var int|NULL $id
      */
     public function edit(Request $request,$id=NULL)
     {
          
          if ($request->isMethod('post'))
          {
               $form = new AddPcForm();
               $pc = $form->buildPc($request);
               
               if($this->pc->updatePc($pc))
               {
                    return redirect()->back()->with('message', 'Успешно обновлено');
               } else {
                    return redirect()->back()->with('message','Во время обновления возникли ошибки');
               } 
          }
          return view('pc/edit',  ['pc' => $this->pc->getPc($id),
                                   'message'=>Session::get('message')]);
     }

}

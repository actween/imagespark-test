<?php namespace App\Http\Db;

use DB;
use App\Http\Model\Pc;
use App\Http\Model\Mac;

class PcDb{
     
     /**
      * 
      * Функция возвращает список комьютеров полученых от БД
      * 
      * @return array|Pc
      */
     public function getAll()
     {
          $result = array();
          $resultPcs = DB::Table('pc')->get();
          
          foreach ($resultPcs as $resultPc)
          {
               $resultMacs = $this->getMacsForPc($resultPc->pc_id);
               $pc = new Pc();
               $pc->setId($resultPc->pc_id);
               $pc->setDescription($resultPc->description);
               
               foreach ($resultMacs as $resultMac)
               {
                   $pc->addMac($resultMac->mac,$resultMac->active,$resultMac->ip);
               }
          
               $result[] = $pc;
          }
          
          return $result;
     }
     /**
      * 
      * Функция возвращает список комьютеров полученых от БД
      * @var int $id - идентификатор компьютера 
      * @return array|Pc
      */
     public function getPc($id)
     {
          $resultPc      = DB::table('pc')->where('pc_id',$id)->first();
          if(empty($resultPc))
               return NULL;
               
          $resultMacs    = DB::table('mac')->where('pc_id',$resultPc->pc_id)->get();
          $pc = new Pc();
          $pc->setId($resultPc->pc_id);
          $pc->setDescription($resultPc->description);
          
          foreach ($resultMacs as $resultMac)
          {             
               $pc->addMac($resultMac->mac,$resultMac->active,long2ip($resultMac->ip));
          }
          return $pc;
     }
     
     /**
      * 
      * Функция возвращает список мак адресо для выбранного компьютера
      * 
      * @var int $pcId - идентификатор компьютера 
      * @return array|Mac
      */
     public function getMacsForPc($pcId)
     {
          return DB::table('mac')->where('pc_id',$pcId)->get();
     }
     
     /**
      * 
      * Функция добавляет компьютер в БД
      * 
      * @var Pc $pc - Данные о компьютере
      * @return boolean
      */
     public function insertPc(Pc $pc)
     {
          return DB::table('pc')->insertGetId(array('description'=>$pc->getDescription()));
     }
     
     /**
      * 
      * Функция обновляет данные о мак-адресе
      * 
      * @var Mac $mac - Данные о компьютере
      * @return boolean
      */
     public function updateMac(Mac $mac){
          DB::table('mac')->where(array('mac'=>$mac->getMac()))->update(array('active'=>$mac->isActive()));
     }
     
     /**
      * 
      * Функция обновляет данные о компьютере
      * 
      * @var Pc $pc - Данные о компьютере
      * @return boolean
      */
     public function updatePc(Pc $pc)
     {
          DB::table('pc')->where(array('pc_id'=>$pc->getId()))->update(array('description'=>$pc->getDescription()));
     }
     
     /**
      * 
      * Функция удаляет данные о компьютере
      * 
      * @var int $pcId - Данные о компьютере
      * @return boolean
      */
     public function removePc($pcId)
     {
          return DB::table('pc')->where('pc_id',$pcId)->delete();
     }
     
     /**
      * 
      * Функция добавляет мак-адрес к выбранному компьютеру
      * 
      * @var Mac $mac - Данные о Mac-адресе
      * @var int $pcId
      * @return boolean
      */
     public function bindMac(Mac $mac,$pcId)
     {
          DB::table('mac')->insert(array('mac'=>$mac->getMac(),
                                         'ip'=>sprintf('%u', ip2long($mac->getIp())),
                                         'active'=>$mac->isActive(),
                                         'pc_id'=>$pcId));
     }
     
     /**
      * 
      * Функция удаляет неиспользуемые мак-адреса у компьютера
      * 
      * @var array|int $macIds - Данные о Mac-адресах
      * @var int $pcId
      * @return boolean
      */
     public function unbindMac($macIds,$pcId)
     {
          return DB::table('mac')->where(array('pc_id'=>$pcId))->whereNotIn('mac',$macIds)->delete();
     }
     
     /**
      * 
      * Функция Проверяет наличие мак-адреса в БД
      * 
      * @var string $mac - мак-адрес
      * @return boolean
      */
     public function macExists($mac)
     {
          if(!empty(Db::table('mac')->where(array('mac'=>$mac))->first()))
               return true;
          else 
               return false;
     }
     
     /**
      * 
      * Функция ищет максимальный ip в БД, если такого нет - возвращает 192.168.0.1
      * 
      * @return string
      */
     public function generateIp()
     {
          $ip = DB::table('mac')->max('ip');
          if(empty($ip))
               return '192.168.0.1';
          else 
               return long2ip($ip+1);
     }
     

}

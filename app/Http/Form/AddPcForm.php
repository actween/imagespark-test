<?php namespace App\Http\Form;

use App\Http\Model\Pc;
use App\Http\Model\Mac;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddPcForm
{
     /*
      * Функция осущесвляет выборку данных из запроса и в случае их валидности возвращает объект
      * 
      * @var Request $request - объект запроса
      * 
      * @result Pc | boolean
      */
     public function buildPc(Request $request)
     {
          $pc = new Pc();
          $errors = false;
          $pcValidator = Validator::make(
               array(
                    'description' => $request->input('description'),
               ),
               array(
                    'description' => 'required|min:1|max:255',
          ));
          if($pcValidator->passes()){
               $pc->setId($request->input('id',NULL));
               $pc->setDescription($request->input('description'));
          } else {
               $errors = true;
          }

          foreach ($request->input('mac',array()) as $key=>$mac)
          {    
               $macValidator = Validator::make(
                    array(
                         'mac'     => $mac,
                         'active'  => $request->input('active.'.$key)
                    ),
                    array(
                         'mac'     => array('required','regex:#^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$#'),
                         'active'  => 'required|boolean',
               ));
               if(!empty($mac) && $macValidator->passes() && $pcValidator->passes()){
                    $pc->addMac($mac,$request->input('active.'.$key));
               } else {
               $errors = true;
               }
          }
          if(!$errors)
               return $pc;
          else 
               return false;
     }
}

<?php
namespace App\Http\Model;

class Mac
{
     protected $mac;
     protected $ip;
     protected $active;
     
     public function setMac($mac)
     {
          $this->mac = $mac;
     }
     
     public function getMac()
     {
          return $this->mac;
     }
     
     public function setIp($ip)
     {
          $this->ip = $ip;
     }
     
     public function getIp()
     {
          return $this->ip;
     }
     
     public function toggle($bool)
     {
          $this->active = (bool)$bool;
     }
     
     public function isActive()
     {
          return $this->active;
     }
     
          public function validate()
     {
          $validator = Validator::make(
              array(
                  'mac' => $this->mac,
                  'ip' => $this->ip,
                  'active'=>$this->active
              ),
              array(
                  'mac' => array('required','unique:mac','regex:#^[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}#'),
                  'active' => 'required|boolean',
              )
          );
          if($validator->fails())
          { 
               return $validator->fails;
          } else {
               return true ;
          }
     }
}

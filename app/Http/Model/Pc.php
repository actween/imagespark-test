<?php
namespace App\Http\Model;

use App\Http\Model\Mac;

class Pc
{
     protected $id;
     protected $description;
     protected $mac = array(); 
     
     
     public function setId($id)
     {
          $this->id = $id;
     }
     
     public function getId()
     {
          return $this->id;
     }
     
     public function setDescription($description)
     {
          $this->description = $description;
     }
     
     public function getDescription()
     {
          return $this->description;
     }
     
     public function getMacs()
     {
          return $this->mac;
     }
     
     public function countMacs()
     {
          return count($this->mac);
     }
     
     public function getMac($position)
     {
          return $this->mac[$position];
     }
     
     public function addMac($macadd,$active,$ip=NULL)
     {
          $mac = new Mac();
          $mac->setMac($macadd);
          $mac->toggle($active);
          $mac->setIp($ip);
          array_push($this->mac,$mac);
     }
     
     public function removeMac($position)
     {
          unset($this->mac[$position]);
     }
}

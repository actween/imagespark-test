<?php
namespace App\Http\Service;

use App\Http\Model\Pc;
use App\Http\Model\Mac;
use Illuminate\Support\Facades\Validator;
use App\Http\Db\PcDb;

class PcService
{    
     protected $pc;
     protected $validationErrors = array();
     
     public function __construct(PcDb $pc)
     {
          $this->pc = $pc;
     }
     
     /**
      * 
      * Функция возвращает список компьютеров в БД
      * 
      * @return array|Pc
      */
     public function getAll()
     {
          return $this->pc->getAll();
     }
     
     /**
      * 
      * Функция возвращает список компьютеров в БД
      * 
      * @var int $id - идентификатор компьютера
      * @return Pc
      */
     public function getPc($id)
     {
          return $this->pc->getPc($id);
     }
     /**
      * 
      * Функция проверят данные на соотвествие правилам и отправляет на сохранение в БД
      * 
      * @var Pc $pc - идентификатор компьютера
      * @return boolean
      */
     public function addPc(Pc $pc)
     {
          $pcValidator = Validator::make(
              array(
                  'description' => $pc->getDescription(),
              ),
              array(
                  'description' => 'required|min:1|max:255',
              ));
          array_merge($this->validationErrors,$pcValidator->failed());
          if($pcValidator->passes() && $pc->countMacs()>0)
          {
               $pcId = $this->pc->insertPc($pc);
               $pc->setId($pcId);
               $macInserted = false;
               foreach ($pc->getMacs() as $mac)
               {
                    if(empty($mac->getIp()))
                         $mac->setIp($this->pc->generateIp());
               
                    $macValidator = Validator::make(
                         array(
                              'mac'     => $mac->getMac(),
                              'ip'      => $mac->getIp(),
                              'active'  => $mac->isActive()
                         ),
                         array(
                              'mac'     => array('required','unique:mac'),
                              'ip'      => array('required','unique:mac,ip'),
                              'active'  => 'required',
                    ));
                    array_merge($this->validationErrors,$macValidator->failed());
                    if($macValidator->passes() && $pcValidator->passes())
                    {
                         $this->pc->bindMac($mac,$pc->getId());
                         $macInserted = true;
                    }

               }
               if($macInserted ==false){
                    $this->pc->removePc($pc->getId());
                    return false;
               }
          } else {
               return false;
          }
          return true;
     }
     /**
      * 
      * Функция удаляет компьютер с идентификатором $pcId
      * 
      * @var int $pcId - идентификатор компьютера
      * @return boolean
      */
     public function removePc($pcId)
     {
          if($this->pc->removePc($pcId))
               return true;
          else 
               return false;
     }
     /**
      * 
      * Функция проверяет данные на соотвествие правилам и отправляет на обновление в БД
      * 
      * @var Pc $pc - идентификатор компьютера
      * 
      * @return boolean
      */
     public function updatePc(Pc $pc)
     {
          $pcValidator = Validator::make(
              array(
                  'description' => $pc->getDescription(),
              ),
              array(
                  'description' => 'required|min:1|max:255',
              ));
          array_merge($this->validationErrors,$pcValidator->failed());
          if($pcValidator->passes() && $pc->countMacs()>0)
          {
               $this->pc->updatePc($pc);
               $macInserted = false;
               $existedMacs = array();
               foreach ($pc->getMacs() as $mac)
               {    
                    if($this->pc->macExists($mac->getMac()))
                    {
                         $this->pc->updateMac($mac);
                         $existedMacs[] = $mac->getMac();
                         $macInserted = true;
                         
                    }
                    else 
                    {
                         if(empty($mac->getIp()))
                              $mac->setIp($this->pc->generateIp());
                              
                         $macValidator = Validator::make(
                         array(
                              'mac'     => $mac->getMac(),
                              'ip'      => $mac->getIp(),
                              'active'  => $mac->isActive()
                         ),
                         array(
                              'mac'     => array('required'),
                              'ip'     => 'required',
                              'active'  => 'required|boolean',
                         ));
                         array_merge($this->validationErrors,$macValidator->failed());
                         if($macValidator->passes() && $pcValidator->passes())
                         {
                              $this->pc->bindMac($mac,$pc->getId());
                              $existedMacs[] = $mac->getMac();
                              $macInserted = true;
                         }
                    }
               }
               
               $this->pc->unbindMac($existedMacs,$pc->getId());
               return true;
          }
         
     }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('pc',              'PcController@index');
Route::get('pc/{id}',         'PcController@show')->where('id', '[0-9]+');
Route::match(['get', 'post'],'pc/add',          'PcController@add');
Route::match(['get', 'post'],'pc/edit/{id}',  'PcController@edit')->where('id', '[0-9]+');
Route::get('pc/remove/{id}',  'PcController@remove')->where('id', '[0-9]+');
Route::get('/', 'WelcomeController@index');
Route::get('home', 'HomeController@index');
/*
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);*/

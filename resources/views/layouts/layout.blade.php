<!DOCTYPE html>
<html lang="en">
  <head>
     <title>Laravel5 - тестовое задание</title>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    @section('script')
    @show
  </head>
  <body>
     <div class="container">
          <header class="header">
               
               <nav>
                    @section('navigation')
                    @show
               </nav>
               <h3 class="text-muted">
                    @section('pagetitle');
                    @show
               </h3>
          </header>
          <main>
               <section>
                    @section('message')
                    @show
               </section>
               <article>
                    @yield('content')
               </article>
          </main>
          <footer></footer>
     </div>
  </body>
</html>

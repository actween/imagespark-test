@extends('layouts.layout')
@section('navigation')
<ul class="nav nav-pills pull-right">
     <li><a class="btn btn-xs btn-info" href="/pc/add">Добавить</a></li>
     <li></li>
     <li class="active"><a class="btn btn-xs btn-info" href="/pc/">Просмотр списка</a></li>
</ul>
@stop
@section('script')
     <script>
          var j = jQuery.noConflict();   
          j(document).ready(function () {
                    j('.add-mac').click(function(){
                         j(this).before(function(){
                              return j(this).prev().append('<section class="macs"><label>MAC: <input name="mac[]" type="text" value=""  pattern=^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$></label> <label>Активно: <select name="active[]"><option value="1">Активно</option><option value="0">Не активно</option></select></label> <a href="#" class="rm-mac btn btn-default">Удалить</a></section>');
                         });
                         return false;
                    });
                    

               });
          j(document).on("click", ".rm-mac", function(){
               j(this).parent().remove();
          });
     </script>
@stop
@section('pagetitle')
     Редактировать запись (id {{ $pc ->getId()}} )
@stop
@section('content')
<form method="POST" action="/pc/edit/{{$pc->getId()}}">
     <div class="form-group">
          <label>Описание 
               <textarea class="form-control" rows="3" name="description">{{$pc->getDescription()}}</textarea>
          </label>
     </div>
     <section class="macs">
          <p class="text-muted">MAC в формате 3D:F2:C9:A6:B3:4F или 3D-F2-C9-A6-B3-4F</p>
     @foreach ($pc->getMacs() as $mac)
     <div class="form-group">
                    
               <label>MAC: 

                    <input name="mac[]" type="text" value="{{$mac->getMac()}}" pattern="^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$">
          
               </label> 
               <label>Активно: 
                    <select name="active[]">
                         <option value="1" @if (!empty($mac->isActive())) {{'selected=selected'}} @endif > Активно</option>
                         <option value="0" @if (empty($mac->isActive())) {{'selected=selected'}} @endunless >Не активно</option>
                    </select>
               </label>
               <a href="#" class="rm-mac btn btn-default">Удалить</a>
     </div>
     @endForeach
     </section>
     <a href=#" class="add-mac btn btn-default">Добавить МАС</a>
     <button type="submit" class="btn btn-default">Сохранить</button>
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
     <input type="hidden" name="id" value="{{$pc->getId()}}">

</form>
@stop

@extends('layouts.layout')
@section('navigation')
<ul class="nav nav-pills pull-right">
     <li><a class="btn btn-xs btn-info" href="/pc/add">Добавить</a></li>
     <li class="active"><a class="btn btn-xs btn-info" href="/pc/">Просмотр списка</a></li>
</ul>
@stop
@section('pagetitle')
     Компьютер (id {{ $pc ->getId()}} )
@stop
@section('content')
<article>
     <ul class="list-inline">
                    <li style="display: inline-block; list-style:none;"><a class="btn btn-xs btn-info" href="/pc/edit/{{$pc->getId()}}">Редактирование</a></li>
                    <li style="display: inline-block; list-style:none;"><a class="btn btn-xs btn-info" href="/pc/remove/{{$pc->getId()}}">Удаление</a></li>
               
               </ul>
     <section> {{ $pc->getDescription() }}</section>
     <section>
          <ul>
               @foreach ($pc->getMacs() as $mac)
                    <li style="list-style:none">
                         <ul>
                              <li style="list-style:none">MAC-адрес: {{$mac->getMac()}} </li>
                              <li style="list-style:none">IP-адрес: {{$mac->getIp()}} </li>
                              <li style="list-style:none">Активен:
                                   @if($mac->isActive())
                                        Активен
                                   @else
                                        Не активен 
                                   @endif 
                              </li>
                         </ul>
                    </li>
               @endforeach
          </ul>
     </section>
</article>
@stop

@extends('layouts.layout')
@section('navigation')
<ul class="nav nav-pills pull-right">
     <li><a class="btn btn-xs btn-info" href="/pc/add">Добавить</a></li>
     <li class="active"><a class="btn btn-xs btn-info" href="/pc/">Просмотр списка</a></li>
</ul>
@stop
@section('message')
     {{$message}}
@stop
@section('pagetitle')
     Список Компьютеров
@stop
@section('content')
     <ul>
          @foreach ($pcs as $pc)
          <li style="list-style:none">
               <section style="float:left">ID: {{$pc->getId()}}</section>
               <ul class="list-inline">
                    <li style="display: inline-block;"><a class="btn btn-xs btn-info" href="/pc/{{$pc->getId()}}">Просмотр</a></li>
                    <li style="display: inline-block;"><a class="btn btn-xs btn-info" href="/pc/edit/{{$pc->getId()}}">Редактирование</a></li>
                    <li style="display: inline-block;"><a class="btn btn-xs btn-info" href="/pc/remove/{{$pc->getId()}}">Удаление</a></li>
               </ul>
               <section><p> {{$pc->getDescription()}}</p></section>
          </li>
          @endforeach
          </ul>
     </section>

@stop
